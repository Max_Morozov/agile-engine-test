<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 30px;
            }


        </style>
        <link href="https://nightly.datatables.net/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-2.2.4/dt-1.10.15/b-1.3.1/b-colvis-1.3.1/kt-2.2.1/r-2.1.1/rg-1.0.0/se-1.2.2/datatables.js"></script>
        <script type="text/javascript" src="/js/index.js"></script>
    </head>
    <body>
    <div class="flex-center position-ref ">
            <div class="content">
                <div class="title m-b-md">
                    Products List
                </div>

                <table id="product" class="display">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Active Discount</th>
                        <th>Price with Discount</th>
                    </tr>
                    </thead>
                </table>

                <div class="title m-b-md">
                    Vouchers List
                </div>
                <table id="voucher" class="display">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Start At</th>
                        <th>End At</th>
                        <th>Discount</th>
                    </tr>
                    </thead>
                </table>
                <div class="postman-run-button"
                     data-postman-action="collection/import"
                     data-postman-var-1="fdc57418b0c597ea4d39"></div>
                <script type="text/javascript">
                    (function (p,o,s,t,m,a,n) {
                        !p[s] && (p[s] = function () { (p[t] || (p[t] = [])).push(arguments); });
                        !o.getElementById(s+t) && o.getElementsByTagName("head")[0].appendChild((
                            (n = o.createElement("script")),
                                (n.id = s+t), (n.async = 1), (n.src = m), n
                        ));
                    }(window, document, "_pm", "PostmanRunObject", "https://run.pstmn.io/button.js"));
                </script>
            </div>
        </div>
    </body>
</html>
