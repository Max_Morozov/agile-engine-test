<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * An Eloquent Model: 'Voucher'
 *
 * @property integer $id
 * @property \DateTime $startAt
 * @property \DateTime $endAt
 * @property Discount $discount
 * @property-read \Illuminate\Database\Eloquent\Collection|Product[] $products
 */
class Voucher extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    public $with = [
        'discount'
    ];
    /**
     * @var array
     */
    public $fillable = [
        'startAt',
        'endAt'
    ];
    /**
     * @var array
     */
    public $hidden = [
        'discount_id'
    ];

    /**
     * @var string
     */
    protected $table = 'vouchers';

    /**
     * Voucher constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function discount()
    {
        return $this->belongsTo(Discount::class, 'discount_id');
    }
}
