<?php

namespace App\Http\Controllers;

use App\Discount;
use App\Http\Requests\VoucherRequest;
use App\Voucher;
use Illuminate\Http\Request;

/**
 * Class VoucherController
 * @package App\Http\Controllers
 */
class VoucherController extends Controller
{
    /**
     * Get all
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        $vouchers = Voucher::all();
        return $vouchers;
    }

    /**
     * Get one
     *
     * @param Voucher $voucher
     * @return Voucher
     */
    public function show(Voucher $voucher)
    {
        return $voucher;
    }

    /**
     * Create voucher
     *
     * @param VoucherRequest $request
     * @return Voucher
     */
    public function store(VoucherRequest $request)
    {
        $voucher = new Voucher($request->all());
        if ($request->has('discount')) {
            $discount = Discount::find($request->get('discount'));
            $voucher->discount()->associate($discount);
        }
        $voucher->save();
        return $voucher;
    }

    /**
     * Update voucher
     * @param VoucherRequest $request
     * @param Voucher $voucher
     * @return Voucher
     */
    public function update(VoucherRequest $request, Voucher $voucher)
    {
        $voucher->update($request->all());
        if ($request->has('discount')) {
            $discount = Discount::find($request->get('discount'));
            $voucher->discount()->associate($discount);
        }
        $voucher->save();
        return $voucher;
    }

    /**
     * Remove voucher
     *
     * @param Voucher $voucher
     * @return string
     */
    public function destroy(Voucher $voucher)
    {
        $voucher->products()->detach($voucher->products()->allRelatedIds());
        $voucher->delete();
        return 'ok';
    }
}
