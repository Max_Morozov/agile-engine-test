<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Product;
use App\Voucher;
use Illuminate\Http\Request;

/**
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
    /**
     * Get all
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        $products = Product::all();
        return $products;
    }

    /**
     * Get one
     *
     * @param Product $product
     * @return Product
     */
    public function show(Product $product)
    {
        return $product;
    }

    /**
     * Create product
     *
     * @param ProductRequest $request
     * @return Product
     */
    public function store(ProductRequest $request)
    {
        $product = new Product($request->all());
        $product->save();
        return $product;
    }

    /**
     * Update Product
     *
     * @param ProductRequest $request
     * @param Product $product
     * @return Product
     */
    public function update(ProductRequest $request, Product $product)
    {
        $product->update($request->all());
        $product->save();
        return $product;
    }

    /**
     * Remove product
     *
     * @param Product $product
     * @return string
     */
    public function destroy(Product $product)
    {
        $product->vouchers()->detach($product->vouchers()->allRelatedIds());
        $product->delete();
        return 'ok';
    }

    /**
     * Add voucher to product
     *
     * @param Product $product
     * @param Voucher $voucher
     * @return Product
     */
    public function addVoucher(Product $product, Voucher $voucher)
    {
        if (false == $product->vouchers->contains($voucher)) {

            $product->vouchers()->save($voucher);
            $product->vouchers->add($voucher);
        }
        return $product;
    }

    /**
     * Remove voucher from product
     *
     * @param Product $product
     * @param Voucher $voucher
     * @return Product
     */
    public function removeVoucher(Product $product, Voucher $voucher)
    {
        $product->vouchers()->detach($voucher);
        $product->vouchers->forget($product->vouchers->search($voucher));
        return $product;
    }

    /**
     * Get all Vouchers
     *
     * @param Product $product
     * @return Voucher[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllVouchers(Product $product)
    {
        return $product->allVouchers();
    }
}
