<?php

namespace App\Http\Controllers;

use App\Discount;
use App\Http\Requests\DiscountRequest;
use Illuminate\Http\Request;

class DiscountController extends Controller
{
    /**
     * Get all
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        $discounts = Discount::all();
        return $discounts;
    }

    /**
     * Get one
     *
     * @param Discount $discount
     * @return Discount
     */
    public function show(Discount $discount)
    {
        return $discount;
    }



}
