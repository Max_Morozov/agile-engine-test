<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * An Eloquent Model: 'Discount'
 *
 * @property integer $id
 * @property integer $percent
 */
class Discount extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var array
     */
    public $fillable =[
        'percent'
    ];

    /**
     * @var string
     */
    protected $table = 'discounts';

}