<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * An Eloquent Model: 'Product'
 *
 * @property integer $id
 * @property string $name
 * @property float $price
 * @property float $totalDiscount
 * @property float $totalPrice
 * @property-read Collection|Voucher[] $vouchers
 */
class Product extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var array
     */
    public $fillable = [
        'name',
        'price',
    ];
    /**
     * @var array
     */
    public $appends = [
        'totalDiscount',
        'totalPrice'
    ];

    /**
     * @var string
     */
    protected $table = 'products';
    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @return BelongsToMany
     */
    public function vouchers()
    {
        return $this->belongsToMany(Voucher::class);
    }

    /**
     * Product constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $now = new \DateTime();

        $this->with = [
            'vouchers' => function ($query) use ($now) {
                $query
                    ->where('startAt', '<', $now)
                    ->where('endAt', '>=', $now);
            },
        ];
    }

    /**
     * @return Collection|Voucher[]
     */
    public function allVouchers()
    {
        return $this->vouchers()->get();
    }

    /**
     * @return int
     */
    public function getTotalDiscountAttribute()
    {
        $now = new \DateTime();

        $discount = $this->vouchers()
            ->where('startAt', '<', $now)
            ->where('endAt', '>=', $now)
            ->join('discounts as d', 'd.id', '=', 'vouchers.discount_id')
            ->sum('d.percent');
        return $discount > 60 ? 60 : $discount;
    }

    /**
     * @return float|int
     */
    public function getTotalPriceAttribute()
    {
        return (100 - $this->totalDiscount) / 100 * $this->price;
    }
}
