<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
    Route::get('products/{product}/vouchers', 'ProductController@getAllVouchers')
        ->where('product', '[0-9]+');
    Route::post('products/{product}/voucher/{voucher}', 'ProductController@addVoucher')
        ->where('product', '[0-9]+')
        ->where('voucher', '[0-9]+');
    Route::delete('products/{product}/voucher/{voucher}', 'ProductController@removeVoucher')
        ->where('product', '[0-9]+')
        ->where('voucher', '[0-9]+');

    Route::resource('products', 'ProductController', ['except' => [
        'create', 'edit'
    ]]);

    Route::resource('discounts', 'DiscountController', ['only' => [
        'index', 'show'
    ]]);
    Route::resource('vouchers', 'VoucherController', ['except' => [
        'create', 'edit'
    ]]);

});
