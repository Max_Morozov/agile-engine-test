
## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb combination of simplicity, elegance, and innovation give you tools you need to build any application with which you are tasked.

## Learning Laravel

Laravel has the most extensive and thorough documentation and video tutorial library of any modern web application framework. The [Laravel documentation](https://laravel.com/docs) is thorough, complete, and makes it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 900 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for helping fund on-going Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](http://patreon.com/taylorotwell):

- **[Vehikl](http://vehikl.com)**
- **[Tighten Co.](https://tighten.co)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Styde](https://styde.net)**
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).


## Start project

check your PC for docker and docker-compose

`docker -v`

`docker-compose -v`

and run command `./deploy.sh` last line output like:

/agile-nginx -  172.20.0.5 -  80/tcp 

/agile-phpmyadmin -  172.20.0.4 -  80/tcp 

/agile-php -  172.20.0.3 -  9000/tcp 

/agile-mysql -  172.20.0.2 -  3306/tcp 

172.20.0.5 - open in your browser and see site

Collection for Posman you can find here [![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/fdc57418b0c597ea4d39#?env%5BangileEngine%20local%5D=W3siZW5hYmxlZCI6dHJ1ZSwia2V5IjoidXJsIiwidmFsdWUiOiJodHRwOi8vMTcyLjIwLjAuNS9hcGkvdjEvIiwidHlwZSI6InRleHQifSx7ImVuYWJsZWQiOmZhbHNlLCJrZXkiOiJ0b2tlbiIsInZhbHVlIjoiQmVhcmVyIGV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSlNVekkxTmlJc0ltcDBhU0k2SWpBd1pqWTJOR1kzWW1RMk1qVTRZekV6TmpCbFlXRmhNV05tTUdZeU1HRmxZek00WW1VMVlqZGtaR0U1TlRZMU5tWXhNVFl4T1dVelpEUTJOak5sTldFMk5qVTBNalEwWmpoak9USXhNRE00SW4wLmV5SmhkV1FpT2lJeUlpd2lhblJwSWpvaU1EQm1OalkwWmpkaVpEWXlOVGhqTVRNMk1HVmhZV0V4WTJZd1pqSXdZV1ZqTXpoaVpUVmlOMlJrWVRrMU5qVTJaakV4TmpFNVpUTmtORFkyTTJVMVlUWTJOVFF5TkRSbU9HTTVNakV3TXpnaUxDSnBZWFFpT2pFME9EWTBOemc0TnpRc0ltNWlaaUk2TVRRNE5qUTNPRGczTkN3aVpYaHdJam94TlRFNE1ERTBPRGMwTENKemRXSWlPaUl4TWlJc0luTmpiM0JsY3lJNlcxMTkuYTA1UXhyenNOcTFNeXJnRWdRMFJMY1ZBaW56dE9kS2tsMmt6bmx0LUNtMHN1WHU4eExkdk05Zk01b0VrVTVVZnBpRnJjNGtHS0JVZ0VFVUp1VWZHQXJQeExlT0tzQXl2WHN3ZS1penRfYzRSMUVab0hrN0F4VTRpWkFsUDNQNnl0dmtfbGRtYVFfTmhwYThrcG5HNzNnM2NtamZuc0wyUU1KT29PbGZ1Wm10YnB2OEh3ejFPVzlOczBScXlVUld6N05oQ3pXREo2cnhEYXlwYjFTQUpoRkpKcFlQd1lqbzJVRVZ4V0JxdERPbEpkd1V4MTZ6SnYtV3lDRUdpMzc2ZUFFcGIyS1A2MFE2ZWlpT3hZa0QweV9kRkdYMjNWeTlZRXdhMmpEZll1dXZEdGhmeEhyYXNvMjJiSnBqc2tEV1ZhdG5rN3FwcldQT0EwbnRtMlhXY19WSkcxYy1LTVR5VjdJby1TalJFbXl5ZkxNNUh2T3pWOXdUVmFFOEhMdlJucG01VVhlWGdxUk5FS1FYSXRabE1BZlprMkJZMlpodTZlY3BfSjVfQVdEekJhMWlYQWZWUFhyZ1ZOMVlLdmxqZzRxNTBiVlFRRGRGd3ZzWG9wdlltSjlMb3RPV3prMkdEY2Q0LTV3VnZyYmJEc1I3cHg5ZG5KbGFCclRRT2pxTVpMMVZRbmMwTTdfU293TXBjTjlWMDZ6OTcxS0p4U0Rrc2dyemZ6WG9hUHdXWnNjTlhZTmhrSUxFUUtGMkdSaUxfRkZfSUhablc5cF85TjBVU0JqWEFhQjRucUprMVhfandPR1ZNTGZvNHV4ZU5Ec3dPdmh1X2RiZUdfOGk5UkpFN3NJTDJ0MFVmekpOTjJxRHNQRDBDbkVocjlCaTNVaklHWTJmNmxRYWo1WjQiLCJ0eXBlIjoidGV4dCJ9XQ==)
