#!/usr/bin/env bash
docker-compose build;
docker-compose stop;
docker-compose up -d;
sleep 1;
docker-compose exec -T php cp .env.example  .env;
docker-compose exec -T php chmod 777 .env;
docker-compose exec -T php chgrp -R www-data storage bootstrap/cache;
docker-compose exec -T php chmod -R ug+rwx storage bootstrap/cache;
docker-compose exec -T php composer install --quiet;
docker-compose exec -T php php artisan migrate --seed -n -q;

docker inspect --format='{{.Name}} - {{range .NetworkSettings.Networks}} {{.IPAddress}}{{end}} - {{range $p, $conf := .NetworkSettings.Ports}} {{$p}} {{end}}' $(docker ps -aq)|grep agile