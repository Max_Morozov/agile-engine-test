/**
 * Created by maxim on 7/11/17.
 */
$(document).ready(function() {

    $('#product').DataTable({
        ajax:
    {
        "url":"/api/v1/products",
        "type" : "get",
        "dataType" : "json",
        dataSrc: '',
    },
        columns: [
            { "data": "id" },
            { "data": "name" },
            { "data": "price" },
            { "data": "totalDiscount" },
            {  "data": "totalPrice"}
        ],
        select: true,

    });
    $('#voucher').DataTable({
        ajax:
            {
                "url":"/api/v1/vouchers",
                "type" : "get",
                "dataType" : "json",
                dataSrc: '',
            },
        columns: [
            { "data": "id" },
            { "data": "startAt" },
            { "data": "endAt" },
            { "data": "discount.percent" },
        ],
        select: true,

    });
} );
