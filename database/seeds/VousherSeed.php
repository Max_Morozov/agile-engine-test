<?php
/**
 * Created by IntelliJ IDEA.
 * User: maxim
 * Date: 7/6/17
 * Time: 11:54 PM
 */

namespace Illuminate\Database\Seeds;

use App\Discount;
use App\Product;
use App\Voucher;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VousherSeed extends Seeder
{
    public function run(){
        DB::table('vouchers')->delete();
        Voucher::create(['startAt'=>new \DateTime('-5 hours'),'endAt'=>new \DateTime('+5 hours')])->discount()->associate(2)->save();
        Voucher::create(['startAt'=>new \DateTime('-3 hours'),'endAt'=>new \DateTime('+8 hours')])->discount()->associate(4)->save();
        Voucher::create(['startAt'=>new \DateTime('-5 hours'),'endAt'=>new \DateTime('-1 hours')])->discount()->associate(1)->save();
    }

}