<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(\Illuminate\Database\Seeds\DiscountSeed::class);
         $this->call(\Illuminate\Database\Seeds\VousherSeed::class);
         $this->call(\Illuminate\Database\Seeds\ProductSeed::class);
    }
}
