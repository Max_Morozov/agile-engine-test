<?php
/**
 * Created by IntelliJ IDEA.
 * User: maxim
 * Date: 7/6/17
 * Time: 11:54 PM
 */

namespace Illuminate\Database\Seeds;

use App\Discount;
use App\Product;
use App\Voucher;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeed extends Seeder
{
    public function run(){
        DB::table('products')->delete();
        Product::create(['name'=>'Product 1','price'=>10])
            ->vouchers()
            ->saveMany(Voucher::findMany([1,3]));
        Product::create(['name'=>'Product 2','price'=>20])
            ->vouchers()
            ->saveMany(Voucher::findMany([3,2]));
        Product::create(['name'=>'Product 3','price'=>30])
            ->vouchers()
            ->saveMany(Voucher::findMany([2,1]));
    }

}