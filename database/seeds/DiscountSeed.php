<?php
/**
 * Created by IntelliJ IDEA.
 * User: maxim
 * Date: 7/6/17
 * Time: 11:54 PM
 */

namespace Illuminate\Database\Seeds;

use App\Discount;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DiscountSeed extends Seeder
{
    public function run(){
        DB::table('discounts')->delete();
        Discount::create(['percent'=>'10']);
        Discount::create(['percent'=>'15']);
        Discount::create(['percent'=>'20']);
        Discount::create(['percent'=>'25']);
    }

}