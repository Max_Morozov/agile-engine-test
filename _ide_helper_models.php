<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * An Eloquent Model: 'Voucher'
 *
 * @property integer $id
 * @property \DateTime $startAt
 * @property \DateTime $endAt
 * @property-read \Illuminate\Database\Eloquent\Collection|Product[] $products
 * @property int $discount_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereDiscountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voucher whereStartAt($value)
 */
	class Voucher extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * An Eloquent Model: 'Product'
 *
 * @property integer $id
 * @property string $name
 * @property float $price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product wherePrice($value)
 */
	class Product extends \Eloquent {}
}

namespace App{
/**
 * An Eloquent Model: 'Discount'
 *
 * @property integer $id
 * @property integer $percent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Discount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Discount wherePercent($value)
 */
	class Discount extends \Eloquent {}
}

